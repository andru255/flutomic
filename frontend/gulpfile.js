var gulp          = require("gulp");
var exec          = require("child_process").exec;
var dockerVars    = require("./docker-vars");
var dockerAdapter = require("./docker-adapter");
var generator = new dockerAdapter();
generator.setDefaultOptions(dockerVars);
var executeMicroTask = generator.generateSentence;

gulp.task("sprites", function(cb){
    exec(executeMicroTask({
        //image: "andru255/micro-sprites",
        image: "docker-sprites",
        tasks:{
            folder: "gulp-sprites"
        },
        instruccions: "gulp"
    }), function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
  });
});

gulp.task("styles", function(cb){
    exec(executeMicroTask({
        image: "andru255/micro-styles",
        tasks:{
            folder: "gulp-styles"
        },
        instruccions: "gulp"
    }), function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task("views", function(cb){
    exec(executeMicroTask({
        image: "micro-views",
        tasks:{
            folder: "gulp-views"
        },
        instruccions: "gulp"
    }), function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task("watch", function(cb){
    gulp.watch("src/**/*.styl", ["styles"]);
    gulp.watch("src/**/*.jade", ["views"]);
});
