var UID = "$(id -u)";
var GID = "$(id -g)";
var PWD = "$(pwd)";

var dockerVars = {
  input:{
    host: [ PWD, "/src" ].join(""),
    guest: "/home/dockerfront/input"
  },
  config:{
    host: [ PWD, "/config" ].join(""),
    guest: "/home/dockerfront/config"
  },
  tasks:{
    host: [ PWD, "/tasks" ].join(""),
    folder: "/defaut-task",
    guest: "/home/dockerfront/tasks"
  },
  output:{
    host: [ PWD, "/../public" ].join(""),
    guest: "/home/dockerfront/output"
  }
};

module.exports = dockerVars;
