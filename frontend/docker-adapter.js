var defaultOptions = {};
var mergeOptions = function(objA, objB){
    var _ = {};
    for(var k in objA){
        _[k] = objA[k];
    }
    var isObject = function(element){
        return Object.prototype.toString.call(element) === "[object Object]";
    };
    var overrideParams = function(A, B){
        for(var key in B){
            if(isObject(A[key])){
                overrideParams(A[key], B[key]);
            } else {
                if(typeof B[key] !== "undefined"){
                    A[key] = B[key];
                }
            }
        }
    };
    overrideParams(_, objB);
    return _;
};
var dockerAdapter = function(){};
dockerAdapter.prototype.setDefaultOptions = function(defaults){
    defaultOptions = defaults;
};
dockerAdapter.prototype.getDefaultOptions = function(){
    return defaultOptions;
};
dockerAdapter.prototype.generateSentence = function(options){
    var settings = mergeOptions(defaultOptions, options);
    console.log("settings", settings);
    var sentence = [
        "docker run",
        " --rm",
        " -e DEV_UID=", "$(id -u)", " -e DEV_GID=", "$(id -g)",
        " -v ", settings.config.host, ":", settings.config.guest,
        " -v ", [ settings.tasks.host , "/", settings.tasks.folder].join(""), ":", settings.tasks.guest,
        " -v ", settings.input.host, ":", settings.input.guest,
        " -v ", settings.output.host, ":", settings.output.guest,
        " ", settings.image,
        " ", settings.instruccions].join("");
    console.log("executing...", sentence);
    return sentence;
};

module.exports = dockerAdapter;
