var gulp = require("gulp");
var config = require('/home/dockerfront/config/views.js');
var jade = require("gulp-jade");
var rename = require("gulp-rename");

gulp.task("default", function(){
    gulp.src(config.src)
        .pipe(jade(config.options.jade))
        .pipe(rename(config.options.rename))
        .pipe(gulp.dest(config.build));
});
