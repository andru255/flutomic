var gulp = require("gulp");
var config = require('/home/dockerfront/config/styles.js');
var stylus = require("gulp-stylus");

gulp.task("default", function(){
    gulp.src(config.src)
        .pipe(stylus(config.options))
        .pipe(gulp.dest(config.build));
});
