var srcPath = "/home/dockerfront/input/";
var outPath = "/home/dockerfront/output/";

var GLOBALS = {
    input: {
      path: srcPath,
      assetsPath: srcPath + "static/"
    },
    output: {
      path: outPath,
      assetsPath: outPath + "static/"
    }
};
module.exports = GLOBALS;
