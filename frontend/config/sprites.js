var globalVars = require("./global.js");
var config = {
    src: globalVars.input.assetsPath + "images/*.gif",
    build: {
        stylus: globalVars.input.path + "/stylus/",
        image : globalVars.output.assetsPath + "/img"
    },
    options: {
        algorithm: 'binary-tree',
        imgName: 'demo.png',
        cssName: 'demo.styl',
        imgPath: './demo.png'
    }
};

module.exports=config;
