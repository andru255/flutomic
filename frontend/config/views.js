var globalVars = require("./global.js");
var srcPath = globalVars.input.path + "views/";
var buildPath = globalVars.output.path;
var config = {
    src:[
        srcPath + '*.jade',
        srcPath  + '**/*.jade',
        '!' + srcPath + '_**/*.jade',
        '!' + srcPath + '**/_**/*.jade',
        '!' + srcPath + '**/_**/**/*.jade',
        '!' + srcPath + '**/_*.jade'
    ],
    build: buildPath + "/",
    options: {
       jade:{
            pretty: true
       },
       rename: {
            extname: ".html"
       }
    }
};

module.exports=config;
