var globalVars = require("./global.js");
var srcPath = globalVars.input.path + "styles/";
var buildPath = globalVars.output.path;
var config = {
    src:[
        srcPath + 'layouts/_render/*.styl',
        srcPath + '**/*.styl',
        '!' + srcPath + '**/**/_**/*.styl',
        '!' + srcPath + '_**/*.styl',
        '!' + srcPath + '**/_*.styl'
    ],
    build: buildPath + "css/",
    options: {
        compress: false
    }
};

module.exports=config;
